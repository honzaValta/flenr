export default {
  cyan: "rgba(20, 240, 190, 1)",
  cyanShadow: "rgba(20, 240, 190, 0.6)",
  red: "rgba(255, 50, 110, 1)",
  redShadow: "rgba(255, 50, 110, 0.5)",
  yellow: "rgba(255, 210, 0, 1)",
  yellowShadow: "rgba(255, 204, 0, 0.5)",
  purple: "rgba(200, 50, 255, 1)",
  purpleShadow: "rgba(200, 50, 255, 0.5)",
  green: "rgba(0, 200, 170, 1)",
  greenShadow: "rgba(0, 200, 170, 0.5)",
  blue: "rgba(0, 150, 255, 1)",
  blueShadow: "rgba(0, 150, 255, 0.5)",
  grey: "rgba(230, 230, 230, 1)",
  greyShadow: "rgba(230, 230, 230, 1)",
  white: "white"
};

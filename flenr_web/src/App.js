import React from "react";
import "./App.css";
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducers from "./data/reducers";

import Dashboard from "./components/Dashboard";
import HomePage from "./components/HomePage";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";

const routing = (
	<Router>
		<div>
			<Route exact path="/" component={HomePage} />
			<Route path="/dashboard" component={Dashboard} />
			{/* <Route path="/auth" component={AuthPage} /> */}
		</div>
	</Router>
);

function App() {
	return (
		<Provider store={createStore(reducers)}>
			<Router>
				<div>
					<Route exact path="/" component={HomePage} />
					<Route path="/dashboard" component={Dashboard} />
					{/* <Route path="/auth" component={AuthPage} /> */}
				</div>
			</Router>
			{/* <Dashboard /> */}
			{/* <HomePage /> */}
		</Provider>
	);
}

export default App;

import { SET_CURRENT_EVENT, ADD_EVENT, REMOVE_EVENT, SET_EVENT_TEMPLATE } from "../types";

export const setCurrentEvent = event => {
	return {
		type: SET_CURRENT_EVENT,
		value: event
	};
};

export const addEvent = event => {
	return {
		type: ADD_EVENT,
		value: event
	}
}

export const removeEvent = event => {
	return {
		type: REMOVE_EVENT,
		value: event
	}
}

export const setEventTemplate = event => {
	return {
		type: SET_EVENT_TEMPLATE,
		value: event
	}
}

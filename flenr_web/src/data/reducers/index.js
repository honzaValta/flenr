import { combineReducers } from 'redux';
import EventReducer from './EventReducer';
import AuthReducer from './AuthReducer';

export default combineReducers({
  auth: AuthReducer,
  events: EventReducer,
});

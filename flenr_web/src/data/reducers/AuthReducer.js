

const INITIAL_STATE = { status: "loading", user: { uid: "x" } };

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'SET_USER':
			console.log("Setting user: ", action.value);
			return { ...state, user: action.value };
		default:
			return state;
	}
};
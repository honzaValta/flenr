import {
	SET_CURRENT_EVENT,
	ADD_EVENT,
	REMOVE_EVENT,
	SET_EVENT_TEMPLATE
} from "../types";

const INITIAL_STATE = {
	events: [
		{
			title: "Some event title",
			description: "Some desription",
			startDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
			startTime: "13:00",
			duration: "2h",
			coupons: 42
		},
		{
			title: "Another event title",
			description: "Another desription",
			startDate: new Date(new Date().getTime() + 60 * 60 * 60 * 1000),
			startTime: "19:00",
			duration: "1h",
			coupons: 22
		}
	],
	currentEvent: {
		title: "Some event title",
		description: "Some desription",
		startDate: "21.6",
		startTime: "13:00",
		duration: "2h",
		coupons: 42
	},
	eventTemplate: {
		title: "",
		desccription: ""
	}
};

export default (state = INITIAL_STATE, action) => {
	let events = state.events;

	switch (action.type) {
		case SET_CURRENT_EVENT:
			return { ...state, currentEvent: action.value };
		case ADD_EVENT:
			events.push(action.value);
			return {
				...state,
				events: events,
				eventTemplate: { title: "", desccription: "" }
			};
		case REMOVE_EVENT:
			console.log(action.value, events);
			events.splice(action.value, 1);
			console.log(events);
			return { ...state, events: events };
		case SET_EVENT_TEMPLATE:
			let et = state.eventTemplate;
			et.title = action.value.title;
			et.description = action.value.desccription;
			return { ...state, eventTemplate: et };
		default:
			return state;
	}
};

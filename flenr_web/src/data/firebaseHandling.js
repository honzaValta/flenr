import { fs } from "../constants/FirestoreInit.js";
import * as firebase from "firebase";

const testBiz = {
	bizid: "69SihPn4PdZjUZNiQlP0TfMXkdV2",
	placeName: "Test name",
	address: "Test address, Praha 142 00",
	events: ["VfPKI1uIR31BIac7aooc", "hR8WmVBDCWDQ88YpWpzR"],
	tel: "+420 234 234 432",
	web: "www.google.com",
	location: [50.008388, 14.455758]
};

export const addEvent = event => {
	console.log("uploading");
	fs.collection("events")
		.add({
			...event,
			...testBiz
		})
		.then(function(docRef) {
			console.log("Document written with ID: ", docRef.id);
			fs.collection("businesses")
				.doc(testBiz.bizid)
				.update({
					events: firebase.firestore.FieldValue.arrayUnion(docRef.id)
				});
		})
		.catch(function(error) {
			console.error("Error adding document: ", error);
		});
};

export const getEvents = async (id) => {
	const snapshot = await fs.collection("events").where("ownerId", "==", id).get();
	return { events: snapshot.docs.map(doc => doc.data()) };
}

export const createBusiness = (uid, user) => {
	fs.collection("businesses")
		.doc(uid)
		.set({
			...user
		});
};

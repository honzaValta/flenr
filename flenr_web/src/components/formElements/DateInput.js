import React from "react";
import MomentLocaleUtils, {
	formatDate,
	parseDate
} from "react-day-picker/moment";
import "moment/locale/cs";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";

export const DateInput = props => {
	return (
		<DayPickerInput
			formatDate={formatDate}
			parseDate={parseDate}
			placeholder={props.placeholder}
			onDayChange={props.onChange}
			onKeyDown={e => e.preventDefault()}
			dayPickerProps={{
				disabledDays: {
					before: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
				},
				locale: "cs",
				localeUtils: MomentLocaleUtils
			}}
		/>
	);
};

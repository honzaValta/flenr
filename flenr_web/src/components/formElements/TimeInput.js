import React, { Component } from "react";
import moment from "moment";
import "rc-time-picker/assets/index.css";
import TimePicker from "rc-time-picker";

export const TimeInput = props => {
	return (
		<TimePicker
			defaultValue={moment.utc("14:00", "HH:mm")}
			showSecond={false}
			minuteStep={15}
			onChange={props.onChange}
			allowEmpty={false}
		/>
	);
};

import React, { Component } from "react";
import "rc-slider/assets/index.css";
import Slider, { createSliderWithTooltip } from "rc-slider";

export const SliderInput = props => {
	const SliderWithToolTip = createSliderWithTooltip(Slider);
	return (
		<SliderWithToolTip
			tipFormatter={value => `${value}${props.type}`}
			onChange={props.onChange}
			defaultValue={props.default}
			step={1}
			min={props.min}
			max={props.max}
		/>
	);
};

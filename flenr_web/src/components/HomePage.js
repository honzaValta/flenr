import React, { Component } from "react";
import Colors from "../constants/Colors";
import { Link } from "react-router-dom";

export default class HomePage extends Component {
	render() {
		return (
			<div style={styles.container}>
				<div className="animatedB" style={styles.logo}>
					Valta.
				</div>
				<div style={{ position: "absolute", bottom: 50 }}>
					<Link
						to="/dashboard"
						style={{ color: "white", fontSize: 30, fontWeight: "bold" }}
					>
						Login
					</Link>
				</div>
			</div>
		);
	}
}

const styles = {
	container: {
		display: "flex",
		flex: 1,
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "center",
		width: "100%",
		height: "100%"
	},
	logo: {
		// backgroundColor: Colors.cyan,
		color: "white",
		borderWidth: 10,
		borderColor: Colors.cyan,
		borderStyle: "solid",
		// padding: 50,
		paddingLeft: 50,
		paddingRight: 50,
		paddingTop: 15,
		paddingBottom: 15,
		fontSize: 50,
		fontWeight: "bold",
		// fontFamily: "Roboto",
		margin: 50
		// height: "100%"
		// backgroundColor: Colors.red
	}
};

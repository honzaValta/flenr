import React, { Component } from "react";
import "./Account.css";

export class AccountPanel extends Component {
	render() {
		// console.log(this.state);
		return (
			<div style={{ width: "100%" }}>
				<div
					style={{
						display: "flex",
						justifyContent: "space-between",
						flexWrap: "wrap",
						color: "#555555"
					}}
				>
					<div style={styles.list}>
						<div style={styles.heading}>Muj podnik</div>
						<div style={styles.stats}>
							<div style={styles.bigStat} className="animatedBg">
								<p style={{ fontWeight: "bold", fontSize: 50 }}>42</p>

								<p>kuponu</p>
								<p>vyuzito</p>
							</div>
							<div style={{ display: "flex", flexDirection: "column" }}>
								<div
									style={{
										...styles.smallStat,
										marginBottom: 40
									}}
									className="animatedBg"
								>
									21x Obliben
								</div>
								<div style={styles.smallStat} className="animatedBg">
									prumer na akci: 12
								</div>
							</div>
						</div>
						{/* <div style={{ marginTop: 60 }}>
							<p style={{ fontWeight: "bold", marginBottom: 15 }}>
								El restauranto
							</p>
							<p style={{ marginBottom: 15 }}>Tel: 253984034</p>
							<div style={{ display: "inline" }}>
								Web:{" "}
								<a href="https://www.google.com" target="blank">
									www.google.com
								</a>
							</div>
						</div> */}
					</div>

					<div style={styles.list}>
						<div
							style={{
								marginTop: 60,
								display: "flex",
								flexDirection: "column",
								alignItems: "center",
								justifyContent: "space-evenly"
							}}
						>
							<p style={{ fontWeight: "bold", marginBottom: 15, fontSize: 20 }}>
								El restauranto
							</p>
							<p style={{ marginBottom: 15 }}>Tel: 253984034</p>
							<div style={{ display: "inline" }}>
								Web:{" "}
								<a href="https://www.google.com" target="blank">
									www.google.com
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
const styles = {
	heading: {
		fontWeight: "700",
		fontSize: 45,
		marginBottom: 25
	},
	stats: {
		display: "flex",
		flexDirection: "row",
		flexWrap: "wrap"
	},
	bigStat: {
		// backgroundColor: "pink",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-around",
		minWidth: 100,
		height: 100,
		marginRight: 40,
		marginBottom: 40,
		padding: 10,
		// color: "white",
		borderRadius: 8
		// background: "rgb(131,58,180)",
		// background:
		// 	"linear-gradient(48deg, rgba(131,58,180,0.8015581232492998) 0%, rgba(255,67,67,0.8883928571428571) 87%)"
	},
	smallStat: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "lightblue",
		width: 170,
		height: 40,
		borderRadius: 8
	},
	list: {
		display: "flex",
		flexDirection: "column",
		margin: 10,
		flex: 1,

		minWidth: 260,
		overflow: "auto"
	},
	item: {
		display: "flex",
		justifyContent: "space-between",
		backgroundColor: "rgb(240, 240, 240)",
		borderRadius: 6,
		minHeight: 20,
		padding: 8,
		marginBottom: 15
	},
	title: {
		display: "flex",
		flex: 8,
		marginLeft: 8,
		marginRight: 8,
		flexShrink: 3
	},
	cancelBtn: {
		display: "flex",
		alignSelf: "flex-end",
		width: 25,
		height: 25,
		backgroundColor: "pink",
		border: "none",
		outline: "none",
		borderRadius: 4,
		cursor: "pointer"
	}
};

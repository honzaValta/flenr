import React, { Component } from "react";
import { connect } from "react-redux";
import {
	setCurrentEvent,
	removeEvent,
	setEventTemplate
} from "../../data/actions";
import moment from "moment";

const mapStateToProps = state => ({
	events: state.events.events,
	currentEvent: state.events.currentEvent
});

class HistoryPanel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			events: props.events
		};
	}
	render() {
		this.state.events.sort((ts1, ts2) => {
			if (ts1.startDate > ts2.startDate) return 1;
			if (ts1.startDate < ts2.startDate) return -1;
			return 0;
		});
		console.log("Events: ", this.props.events);
		return (
			<div style={{ width: "100%" }}>
				<div style={styles.heading}>Moje akce</div>
				<div
					style={{
						display: "flex",
						justifyContent: "space-between",
						flexWrap: "wrap",
						color: "#555555"
					}}
				>
					<div style={styles.list}>
						<div
							style={{ overflowY: "scroll", padding: 20, margin: -20 }}
							className="noScrollBar"
						>
							{this.state.events.map((event, index) => {
								return (
									<div key={index} style={styles.item} className="grow">
										<div
											style={{
												display: "flex",
												backgroundColor: "rgba(20, 240, 190, 1)",
												height: "100%",
												padding: 8,
												borderTopLeftRadius: 6,
												borderBottomLeftRadius: 6
											}}
										>
											{moment(event.startDate).format("D.M")}
										</div>
										<span
											style={styles.title}
											onClick={() => this.props.setCurrentEvent(event)}
										>
											{event.title}
										</span>
										<div style={{ display: "flex", margin: 8 }}>
											{event.coupons}
										</div>

										<div
											style={{
												height: "100%",
												display: "flex",
												padding: 8,
												borderTopRightRadius: 6,
												borderBottomRightRadius: 6
											}}
											className="cancelBtn"
											onClick={() => {
												this.props.removeEvent(index);
												this.setState({ events: this.props.events });
											}}
										>
											X
										</div>
									</div>
								);
							})}
						</div>
					</div>

					<div style={styles.detail}>
						<div
							style={{
								...styles.detailField,
								minHeight: 40,
								fontWeight: "bold",
								fontSize: "1.5rem"
							}}
						>
							{this.props.currentEvent.title}
						</div>
						<div
							style={{ ...styles.detailField, minHeight: 50, marginBottom: 30 }}
						>
							{this.props.currentEvent.description}
						</div>
						<div
							style={{
								display: "flex",
								justifyContent: "space-between",
								textAlign: "center",
								marginBottom: 20
							}}
						>
							<div style={{ ...styles.detailField, flex: 4, marginRight: 10 }}>
								{moment(this.props.currentEvent.startDate).format("D.M")}
							</div>
							<div style={{ ...styles.detailField, flex: 2, marginRight: 10 }}>
								{this.props.currentEvent.startTime}
							</div>
							<div style={{ ...styles.detailField, flex: 1 }}>
								{this.props.currentEvent.duration}
							</div>
						</div>
						<div
							style={{
								...styles.detailField,
								alignSelf: "center",
								fontWeight: "bold",
								fontSize: "2rem"
							}}
						>
							{`${this.props.currentEvent.coupons}`}
						</div>
						<button
							className="submitBtn"
							style={{
								marginTop: 20,
								display: "flex",
								justifyContent: "center",
								alignItems: "center"
							}}
							onClick={() => {
								this.props.changeToEvent();
								this.props.setEventTemplate(this.props.currentEvent);
							}}
						>
							Pouzit jako predlohu
						</button>
					</div>
				</div>
			</div>
		);
	}
}
const styles = {
	heading: {
		fontWeight: "700",
		fontSize: 45,
		marginBottom: 25,
		color: "rgb(85, 85, 85)",
		margin: 20,
		marginBottom: 25
	},
	list: {
		display: "flex",
		flexDirection: "column",
		margin: 10,
		flex: 4,
		height: 400,
		minWidth: 260,
		overflow: "visible"
	},
	item: {
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
		backgroundColor: "rgb(240, 240, 240)",
		borderRadius: 6,
		minHeight: 30,
		marginBottom: 50,
		marginRight: 15
	},
	title: {
		display: "flex",
		flex: 8,
		margin: 8,
		flexShrink: 3,
		overflow: "hidden",
		whiteSpace: "nowrap"
	},
	detail: {
		display: "flex",
		flexDirection: "column",
		margin: 10,
		flex: 2,
		height: 400,
		minWidth: 260,
		overflowY: "auto"
	},
	detailField: {
		backgroundColor: "rgb(240, 240, 240)",
		padding: 8,
		borderRadius: 6,
		minHeight: 20,
		marginBottom: 15
	}
};

export default connect(
	mapStateToProps,
	{ setCurrentEvent, removeEvent, setEventTemplate }
)(HistoryPanel);

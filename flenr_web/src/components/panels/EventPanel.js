import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { DateInput, TimeInput } from "../formElements";
// import { addEvent } from "../../data/firebaseHandling";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { fs } from "../../constants/FirestoreInit";
import * as firebase from "firebase";
import { addEvent, setEventTemplate } from "../../data/actions";

const mapStateToProps = state => ({
	eventTemplate: state.events.eventTemplate
});

class EventPanel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: props.eventTemplate.title,
			description: props.eventTemplate.description,
			eventStartDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
			eventStartTime: moment.utc("14:00", "HH:mm").format("HH:mm"),
			duration: 2,
			category: "food",
			coupons: 20,
			touched: {
				title: false,
				description: false,
				date: false,
				time: false,
				duration: false,
				coupons: false
			},
			valid: {
				title: false,
				description: false,
				date: false,
				time: false,
				duration: false,
				coupons: false
			}
		};
	}

	handleChange(key, value) {
		this.setState({ [key]: value });
		let valid = this.state.valid;
		console.log("1", valid);
		if (this.state.title.length >= 3) {
			valid.title = true;
			this.setState({ valid: valid });
		} else {
			valid.title = false;
			this.setState({ valid: valid });
		}

		if (this.state.coupons > 10) {
			valid.coupons = true;
			this.setState({ valid: valid });
		} else {
			valid.coupons = false;
			this.setState({ valid: valid });
		}
		console.log("2", valid);
	}

	validate(field, value) {
		this.setState({ touched: { ...this.state.touched, [field]: true } });
		switch (field) {
			case "title":
				if (value.length > 3) {
					this.setState({ valid: { ...this.state.valid, [field]: true } });
				}
				break;
			case "date":
				if (value > new Date(new Date().getTime() + 24 * 60 * 60 * 1000)) {
				}
				break;
			default:
				break;
		}
	}

	onSubmit() {
		// const startDate = moment(this.state.eventStartDate).format("D.M");
		const startTime = this.state.eventStartTime;
		const eventStart = new Date(
			this.state.eventStartDate + "T" + this.state.eventStartTime
		);
		const eventEnd = new Date(
			eventStart.getTime() + this.state.duration * 60 * 60 * 1000
		);
		this.setState({
			eventStart: firebase.firestore.Timestamp.fromDate(eventStart),
			eventEnd: firebase.firestore.Timestamp.fromDate(eventEnd)
		});

		// console.log("Event : " + JSON.stringify(this.state, null, 4));

		let event = {
			title: this.state.title,
			description: this.state.description,
			startDate: this.state.eventStartDate,
			startTime: startTime,
			duration: this.state.duration + "h",
			coupons: this.state.coupons
		};

		if (event.coupons > 10 && event.title) this.props.addEvent(event);
		this.props.setEventTemplate({ title: "", description: "" });
		this.props.changeToHistory();
		console.log("event: " + JSON.stringify(event, null, 4));
		// addEvent(this.state);
	}

	onTouch(field) {
		this.setState({
			touched: { ...this.state.touched, [field]: true }
		});
	}

	render() {
		// console.log(this.state.valid);
		return (
			<div>
				<div style={styles.heading}>Vytvorit akci</div>
				<form style={styles.container}>
					<div style={styles.formCol}>
						<label>
							Název akce
							<textarea
								required
								type="text"
								maxLength={60}
								minLength={3}
								rows={3}
								value={this.state.title}
								onChange={event =>
									this.handleChange("title", event.target.value)
								}
								// className={this.state.title.length > 3 ? "validInput" : ""}
								style={{ marginTop: 10, marginBottom: 50 }}
								onFocus={() => this.onTouch("title")}
								// onBlur={() => this.state.title.length() > 9 ? this.setState({validTitle: true}) : this.setState({validTitle: false})}
							/>
						</label>
						<label>
							Dodatečné informace
							<textarea
								type="text"
								rows={6}
								value={this.state.description}
								onChange={event =>
									this.handleChange("description", event.target.value)
								}
								style={{ marginTop: 10 }}
							/>
						</label>
					</div>
					<div style={styles.formCol}>
						<label>
							Začátek akce
							<div
								style={{
									display: "flex",
									flexDirection: "row",
									flexWrap: "wrap",
									justifyContent: "space-between",
									width: "100%",
									marginBottom: 50,
									marginTop: 10
								}}
							>
								<div
									style={{ width: "60%", marginRight: 25, marginBottom: 10 }}
								>
									<DateInput
										required
										value={this.state.eventStartDate}
										onChange={value =>
											this.handleChange(
												"eventStartDate",
												moment(value).format("YYYY-MM-DD")
											)
										}
										placeholder={moment(this.state.eventStartDate).format(
											"DD.MM.YYYY"
										)}
										onKeyDown={e => e.preventDefault()}
									/>
								</div>
								<div style={{ width: "25%", minWidth: 60 }}>
									<TimeInput
										required
										value={this.state.eventStartTime}
										onChange={value =>
											this.handleChange("eventStartTime", value.format("HH:mm"))
										}
										onKeyDown={e => e.preventDefault()}
									/>
								</div>
							</div>
						</label>

						<label>
							Doba trvání: {this.state.duration}h
							<div style={{ marginTop: 25, paddingLeft: 5, paddingRight: 5 }}>
								<input
									className="greenRange"
									type="range"
									style={{ width: "100%" }}
									min={1}
									max={24}
									defaultValue={2}
									onChange={event =>
										this.handleChange("duration", event.target.value)
									}
								/>
								{/* <InputRange
									maxValue={24}
									minValue={1}
									value={this.state.duration}
									onChange={value => this.handleChange("duration", value)}
									activeTrack="slider"
								/> */}
							</div>
						</label>
					</div>
					<div style={{ ...styles.formCol, justifyContent: "space-between" }}>
						<label>
							Počet kupónů
							<div style={{ marginTop: 10 }}>
								<input
									required
									type="number"
									max={150}
									min={10}
									value={this.state.coupons}
									onChange={event =>
										this.handleChange("coupons", event.target.value)
									}
									// onKeyDown={e => e.preventDefault()}
									style={{ textAlign: "center", fontSize: 80, width: 180 }}
								/>
							</div>
						</label>
						<button
							style={
								!(this.state.valid.title && this.state.valid.coupons)
									? { backgroundColor: "grey" }
									: {}
							}
							onClick={event => {
								event.preventDefault();
								this.onSubmit();
							}}
							className="submitBtn"
						>
							Vytvorit akci
						</button>
					</div>
				</form>
				{/* <p>{JSON.stringify(this.state, null, 4)}</p> */}
			</div>
		);
	}
}

export default connect(
	mapStateToProps,
	{ addEvent, setEventTemplate }
)(EventPanel);

const styles = {
	heading: {
		fontWeight: "700",
		fontSize: 45,
		marginBottom: 25,
		color: "rgb(85, 85, 85)",
		margin: 20
	},
	container: {
		display: "flex",
		flexWrap: "wrap",
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-around"
	},
	formCol: {
		display: "flex",
		flex: 1,
		flexDirection: "column",
		// justifyContent: "space-between",
		// alignItems: "center",
		width: "30%",
		minWidth: 190,
		height: 300,
		margin: 20
	}
};

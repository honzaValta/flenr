import React, { Component } from "react";
import { AccountPanel, OverviewPanel } from "./panels";
import HistoryPanel from "./panels/HistoryPanel";
import EventPanel from "./panels/EventPanel";
import Colors from "../constants/Colors";
import * as firebase from "firebase";
import { fs } from "../constants/FirestoreInit.js";
import { Link } from "react-router-dom";

export default class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = { currentPanel: "History" };
		// this.getEvents();
	}

	// async getEvents() {
	// 	fs.collection("events").onSnapshot(snapshot => {
	// 		const events = snapshot.docs.map(doc => doc.data());
	// 		events.sort((ts1, ts2) => {
	// 			if (ts1.timeRange[0] > ts2.timeRange[0]) return 1;
	// 			if (ts1.timeRange[0] < ts2.timeRange[0]) return -1;
	// 			return 0;
	// 		});
	// 		this.props.setEvents(events);
	// 		// console.log('Current data: ', events);
	// 	});
	// }

	// checkUser() {
	// 	// firebase.auth().signOut();
	// 	firebase.auth().onAuthStateChanged(async user => {
	// 		if (user) {
	// 			// User is signed in.
	// 			this.getEvents();
	// 			const userInfo = await fetchUser(user.uid);
	// 			this.props.setUser(await userInfo);
	// 			this.props.navigation.navigate("Home");
	// 		} else {
	// 			// No user is signed in.
	// 			this.getEvents();
	// 			// this.fetchUser("ooyGpQKUYTeaFFMY4oy38vXZolE3");
	// 			this.props.navigation.navigate("Auth");
	// 		}
	// 	});
	// }

	panel() {
		switch (this.state.currentPanel) {
			case "Event":
				return (
					<EventPanel
						changeToHistory={() => this.setState({ currentPanel: "History" })}
					/>
				);
			case "Account":
				return <AccountPanel />;
			case "Overview":
				return <OverviewPanel />;
			case "History":
				return (
					<HistoryPanel
						changeToEvent={() => this.setState({ currentPanel: "Event" })}
					/>
				);
			default:
				return <EventPanel />;
		}
	}

	render() {
		return (
			<div style={styles.container}>
				<nav style={styles.header}>
					<div style={styles.logo}>Valta.</div>
				</nav>
				<div style={styles.body}>
					<div style={styles.controlBar}>
						<button
							className="navButton"
							style={styles.panelButton}
							onClick={() => this.setState({ currentPanel: "Event" })}
						>
							Nova akce
						</button>
						<button
							className="navButton"
							style={styles.panelButton}
							onClick={() => this.setState({ currentPanel: "History" })}
						>
							Moje akce
						</button>
						<button
							className="navButton"
							style={styles.panelButton}
							onClick={() => this.setState({ currentPanel: "Account" })}
						>
							Muj podnik
						</button>
					</div>

					<div style={styles.panel}>{this.panel()}</div>
				</div>
				<footer style={styles.footer}>
					<Link to="/" style={{ color: "white" }}>
						Logout
					</Link>
				</footer>
			</div>
		);
	}
}

const styles = {
	container: {
		display: "flex",
		flex: 1,
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "rgba(230, 230, 230, 0)",
		width: "100%"
		// height: "100%"
		// width: "90%",
		// maxWidth: 1200
	},
	navbar: {
		display: "flex"
		// justifyContent: ''
	},
	header: {
		color: "white",
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		// backgroundColor: Colors.cyan,
		width: "100%",
		// height: 40,
		margin: 20,
		marginBottom: 40
	},
	body: {
		display: "flex",
		flex: 1,
		justifyContent: "center",
		width: "100%",
		maxWidth: 1200,
		flexWrap: "wrap"
		// height: "100%"
	},
	panel: {
		display: "flex",
		backgroundColor: "white",
		borderRadius: 8,
		marginBottom: 30,
		minWidth: 310,
		padding: "2%",
		width: "69%"
	},
	controlBar: {
		display: "flex",
		flexDirection: "column",
		marginRight: 20
	},
	panelButton: {
		width: 150,
		marginBottom: 20,
		height: 45,
		borderRadius: 8,
		border: "none",
		outline: "none",
		fontSize: 15,
		fontWeight: "bold"
	},
	logo: {
		fontSize: 50,
		fontWeight: "bold"
	},
	footer: {
		marginBottom: 100
	}
};

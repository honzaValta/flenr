import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import reducers from "./src/data/reducers";
import TestApp from "./testviews/App.js";
// import { store } from './src/redux/app-redux';
import { AppScreens } from "./src/config/router";

export default class App extends React.Component {
	render() {
		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

		return (
			// <TestApp />
			<Provider store={store}>
			  <AppScreens />
			</Provider>
		);
	}
}

/* @flow */

import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { BlurView } from "expo";
import { connect } from "react-redux";

import Colors from "./../constants/Colors";
import { CardButton, Card } from "./basic";
import { setFoodCategory, setDrinkCategory } from "./../data/actions";
import Icon from "react-native-vector-icons/MaterialIcons";
import { withNavigation } from "react-navigation";

const mapStateToProps = state => ({
	category: state.map.category
});

class ControlPanel extends Component {
	render() {
		return (
			<BlurView intensity={90} style={styles.container}>
				<Card style={styles.logo}>
					<Text style={styles.logoText}>logo</Text>
				</Card>
				<View style={styles.buttons}>
					<CardButton
						onPress={() => {
							this.props.navigation.navigate("Favorites");
						}}
						style={{ height: 45, width: 45 }}
						cardStyle={{
							backgroundColor: Colors.red,
							shadowColor: Colors.redShadow,
							borderRadius: 100
						}}
					>
						<Icon name="star" size={25} color="white" />
					</CardButton>
					<CardButton
						onPress={() => {
							this.props.navigation.navigate("Account");
						}}
						style={{ height: 45, width: 45, marginLeft: 10, marginRight: 10 }}
						cardStyle={{
							backgroundColor: Colors.yellow,
							shadowColor: Colors.yellowShadow,
							borderRadius: 100
						}}
					>
						<Icon name="person" size={25} color="white" />
					</CardButton>
					<CardButton
						onPress={() => {
							this.props.navigation.navigate("Search");
						}}
						style={{ height: 45, width: 45 }}
						cardStyle={{
							backgroundColor: Colors.blue,
							shadowColor: Colors.blueShadow,
							borderRadius: 100
						}}
					>
						<Icon name="search" size={25} color="white" />
					</CardButton>
				</View>
			</BlurView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "rgba(255, 255, 255, 0.3)",
		borderRadius: 100,
		padding: 5
	},
	logo: {
		borderRadius: 100,
		backgroundColor: Colors.cyan,
		shadowColor: Colors.cyanShadow,
		width: "40%",
		height: "100%",
		justifyContent: "center",
		alignItems: "center",
		padding: 4,
		marginRight: 10
	},
	logoText: {
		fontSize: 30,
		fontWeight: "800",
		textAlign: "center",
		color: "white"
	},
	buttons: {
		flexDirection: "row",
		justifyContent: "space-between",
		minWidth: 155,
		width: "50%",
		height: "100%"
	}
});

const Cp = withNavigation(ControlPanel);
export default connect(
	mapStateToProps,
	{ setFoodCategory, setDrinkCategory }
)(Cp);

import React from "react";
import { View, Text, StyleSheet } from "react-native";
// import Colors from "./../../constants/Colors";
import { Card } from "./basic";
import Colors from "../constants/Colors";

export const EventCard = props => (
  <Card style={styles.container}>
    <Text style={{ color: "grey" }}>
      {props.duration} / {props.date}
    </Text>
    <Text style={styles.title}>{props.title}</Text>
    <View style={styles.footer}>
      <Text>{props.business}</Text>
      {props.coupons ? (
        <View
          style={{ backgroundColor: Colors.red, borderRadius: 100, padding: 5 }}
        >
          <Text style={{ color: "white", fontWeight: "bold" }}>
            {props.coupons}
          </Text>
        </View>
      ) : (
        ""
      )}
    </View>
  </Card>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "flex-start",
    margin: 10
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    margin: 8,
    marginLeft: 0
  }
});

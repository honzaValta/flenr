/* @flow weak */

import React from 'react';
import {
  Text,
  ScrollView,
} from 'react-native';

const TextView = ({ text }) => (
  <ScrollView>
    <Text style={{ fontSize: 17, }}>{text}</Text>
  </ScrollView>
);

export { TextView };

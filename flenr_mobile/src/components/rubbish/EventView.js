import React, { Component } from "react";
import {
		View,
		StyleSheet,
	} from "react-native";
import SwipeUpDown from 'react-native-swipe-up-down';
import { CalloutHeader, CalloutFull } from '../callout';

const mapStateToProps = state => ({
  currentEvent: state.map.currentEvent
});

class EventView extends Component {
	render() {
		return (
			<SwipeUpDown 
				itemMini={<CalloutHeader />} // Pass props component when collapsed
				itemFull={<CalloutFull />} // Pass props component when show full
				// onShowMini={() => console.log('mini')}
				// onShowFull={() => console.log('full')}
				// onMoveDown={() => console.log('down')}
				// onMoveUp={() => console.log('up')}
				disablePressToShow={false} // Press item mini to show full
			/>
		);
	}
}


import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';

import { resetTimer } from '../../data/actions';

const mapStateToProps = (state) => ({
  time: state.ticket.time,
});

class Timer extends Component {
  constructor(props) {
    super(props);
    // this.startTimer();
    this.state = {
      timer: 30,
      displayTimer: '5:00',
    };
  }

  componentDidUpdate() {
    if (this.props.time === 0) {
      this.props.navigation.navigate('Home');
      this.props.resetTimer();
    }
  }

  render() {
    const { time } = this.props;
    const displayTime = `${~~((time) / 60)} : ${(time) % 60 < 10 ? `0${(time) % 60}` : ((time) % 60)}`;

    return (
      <View>
        <Text style={styles.timer}>{displayTime}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  timer: {
    fontSize: 80,
    fontWeight: '700',
    color: 'white',
  },
});

export default connect(mapStateToProps, { resetTimer })(Timer);

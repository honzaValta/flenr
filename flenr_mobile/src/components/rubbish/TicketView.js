import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import TimerCountdown from "react-native-timer-countdown";
import { Card } from "../basic";
import Colors from "../../constants/Colors";
import { setFoodCategory, setDrinkCategory } from "../../data/actions";

const mapStateToProps = state => ({
  title: state.map.currentEvent.title,
  placeName: state.map.currentEvent.placeName
});

class TicketView extends Component {
  render() {
    const { title, placeName } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          // Title
          <Card>
            <Text>Title</Text>
          </Card>
          // Timer
          <Card>
            <TimerCountdown
              initialMilliseconds={1000 * 20}
              onExpire={() => console.log("")}
              allowFontScaling
              style={{ fontSize: 20 }}
            />
          </Card>
          // Account info
          <Card>
            // Avatar
            <View />
            // Name
            <View>
              <Text>First name</Text>
              <Text>Last name</Text>
            </View>
          </Card>
          // Map/animation check = compass
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  }
});

export default connect(
  mapStateToProps,
  { setFoodCategory, setDrinkCategory }
)(TicketView);

/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';

import Colors from '../../constants/Colors';
import { RoundIconButton, Card} from '../basic';
import { hideCallout, startTimer } from '../../data/actions';

const mapStateToProps = (state) => ({
    currentEvent: state.map.currentEvent,
  });

class FeedView extends Component {

  render() {
    // const { currentEvent } = this.props;
    return (
      <View style={styles.container} >
        <View>

        </View>
        <ScrollView style={{ zIndex: 0, flex: 1 }}>

        </ScrollView>
        <View style={styles.cancelButton} >
          <RoundIconButton onPress={() => this.props.hideCallout()} iconName="clear" iconColor="grey" color='rgba(255, 255, 255, 0.0)' size={25} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default connect(mapStateToProps, { hideCallout, startTimer })(FeedView);

/* @flow */

import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";

import Colors from "../../constants/Colors";
import ControlBar from "../callout/ControlBar";
import { Card } from "../basic";
import { BusinessCard } from "../callout";
import { hideCallout, startTimer } from "../../data/actions";

const mapStateToProps = state => ({
  currentEvent: state.map.currentEvent
});

class CalloutView extends Component {
  render() {
    const { currentEvent } = this.props;
    return (
      <View style={styles.calloutView}>
        <View style={styles.topCallout}>
          <Card
            style={{
              marginBottom: 5,
              padding: 4,
              borderRadius: 6,
              marginLeft: 12
            }}
          >
            <Text>18:00-21:00</Text>
          </Card>
        </View>
        // Callout
        <ScrollView style={styles.container}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              height: "100%"
            }}
          >
            // Callout header
            <View style={styles.header}>
              <Card style={styles.counter}>
                <Text
                  style={{ fontSize: 30, color: "white", fontWeight: "bold" }}
                >
                  42
                </Text>
              </Card>
              <Card style={styles.title}>
                <Text style={{ fontSize: 25 }}>
                  Title asdf dsaf df sdfk ljs lafdkj lsj sadff.
                </Text>
              </Card>
            </View>
            // Description
            <Card style={styles.description}>
              <Text style={{ color: "grey", fontSize: 16 }}>Description</Text>
              <Text style={{ fontSize: 16 }}>{currentEvent.description}</Text>
            </Card>
            // Business
            <BusinessCard />
            // ActionButton
            <ControlBar />
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Ticket");
              }}
              style={{ width: "70%", marginBottom: 50 }}
            >
              <Card style={styles.useButton}>
                <Text
                  style={{ fontWeight: "800", fontSize: 30, color: "white" }}
                >
                  USE
                </Text>
              </Card>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  calloutView: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    height: "100%"
  },
  container: {
    height: "100%",
    backgroundColor: Colors.white,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    padding: 12
  },
  header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
    marginBottom: 16
  },
  topCallout: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    maxHeight: 50
  },
  counter: {
    backgroundColor: Colors.red,
    shadowColor: Colors.redShadow
  },
  title: {
    flex: 1,
    marginLeft: 8
  },
  description: {
    width: "100%",
    marginBottom: 16
  },
  useButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.cyan,
    shadowColor: Colors.cyanShadow,
    borderRadius: 100,
    marginTop: 30
  }

  // cancelButton: {
  //   position: "absolute",
  //   top: 5,
  //   right: 5,
  //   zIndex: 1
  // }
});

export default connect(
  mapStateToProps,
  { hideCallout, startTimer }
)(CalloutView);

// return (
//   <View style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)', flex: 1, flexDirection: 'column' }}>
//     <ScrollView style={{ zIndex: 0, flex: 1 }}>
//       <View style={styles.container}>
//           <View style={styles.title}>
//             <Text style={{ fontSize: 20, fontWeight: 'bold', }} >{currentEvent.title}</Text>
//           </View>
//         <Hr />
//           <View style={styles.name}>
//             <Text style={{ fontSize: 20, fontWeight: 'bold', }} >{currentEvent.placeName}</Text>
//           </View>
//         <Hr />
//           <View style={styles.control} >
//             <ControlBar />
//           </View>
//         <Hr />
//           <View style={styles.fields} >
//             <Text style={{ fontSize: 17, fontWeight: 'bold', }}>Phone</Text>
//             <Text style={{ fontSize: 17 }} >{currentEvent.phone}</Text>
//           </View>
//           <View style={styles.fields} >
//             <Text style={{ fontSize: 17, fontWeight: 'bold', }}>Address</Text>
//             <Text style={{ fontSize: 17, color: 'rgba(0,0,0,0.8)', }}>{currentEvent.address}</Text>
//           </View>
//           <View style={styles.description} >
//             <Text style={{ fontSize: 17, fontWeight: 'bold', }}>Description</Text>
//             <TextView text={currentEvent.description} />
//           </View>
//           <View style={styles.buttons} >
//             <RoundIconButton onPress={() => { this.props.navigation.navigate('Ticket'); this.props.startTimer(); }} text="USE" size={70} color={Colors.cyan} />
//           </View>
//       </View>
//     </ScrollView>
//     <View style={styles.cancelButton} >
//       <RoundIconButton onPress={() => this.props.hideCallout()} iconName="clear" iconColor="grey" color='rgba(255, 255, 255, 0.0)' size={25} />
//     </View>
//   </View>
// );

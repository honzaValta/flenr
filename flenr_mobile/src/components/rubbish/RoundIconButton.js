import React from "react";
import { TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const RoundIconButton = ({
  onPress,
  color = "white",
  iconName,
  size = 44,
  text = " ",
  textColor = "white",
  iconColor = "white"
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        ...styles.buttonStyle,
        backgroundColor: color,
        width: size,
        height: size
      }}
    >
      {iconName ? (
        <Icon name={iconName} size={25} color={iconColor} />
      ) : (
        <Text style={{ color: textColor, fontWeight: "800", fontSize: 20 }}>
          {text}
        </Text>
      )}
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center"
  }
};

export { RoundIconButton };

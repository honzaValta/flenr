/* @flow weak */

import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

const Hr = ({ size = 0.5, color = 'grey', }) => (
  <View style={{ ...styles.container, borderBottomWidth: size, borderColor: color }} />
);

export { Hr };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
    marginBottom: 8,
  },
});

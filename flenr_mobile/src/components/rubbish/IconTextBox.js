/* @flow weak */

import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


const IconTextBox = ({ iconName, iconColor = 'black', boxColor = 'black', iconSize = 35, textSize = 20, textWeight = 'bold', text = 'text' }) => (
  <View style={styles.container}>
    <Icon name={iconName} size={iconSize} color={iconColor} />
    <View style={{ padding: 8, backgroundColor: boxColor, borderRadius: 10, }}>
      <Text style={{ fontSize: textSize, fontWeight: textWeight }}>{text}</Text>
    </View>
  </View>
);

export default IconTextBox;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

import React from "react";
import { Text, StyleSheet } from "react-native";
import { CardButton } from "../basic";
import Colors from "../../constants/Colors";
import { withNavigation } from "react-navigation";

const UseButton = props => (
	<CardButton
		disabled={props.disabled}
		onPress={props.onPress}
		style={{ width: "70%", marginBottom: 50 }}
		cardStyle={props.disabled ? styles.disabledBtn : styles.activeBtn}
	>
		<Text style={props.disabled ? styles.disabledText : styles.activeText}>
			{props.disabled ? "Event is not active" : "USE"}
		</Text>
	</CardButton>
);

export default withNavigation(UseButton);

const styles = StyleSheet.create({
	activeBtn: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: Colors.cyan,
		shadowColor: Colors.cyanShadow,
		borderRadius: 100,
		marginTop: 30,
		marginBottom: 100
	},
	disabledBtn: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: Colors.grey,
		shadowColor: Colors.greyShadow,
		borderRadius: 100,
		marginTop: 30,
		marginBottom: 100
	},
	activeText: {
		fontWeight: "800",
		fontSize: 30,
		color: "white"
	},
	disabledText: {
		fontWeight: "500",
		fontSize: 18,
		color: "black"
	}
});

import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Colors from "./../../constants/Colors";
import { Card } from "../basic";
import Icon from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";

import { addFavorite, removeFavorite } from "../../data/actions";

import {
	addFavoriteBusiness,
	removeFavoriteBusiness
} from "./../../data/firebaseHandling";

const mapStateToProps = state => ({
	user: state.auth.user.uid,
	business: state.map.currentEvent.ownerId,
	favorites: state.auth.user.favorites
});

class BusinessCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			favorite: this.props.favorites.includes(this.props.business)
				? true
				: false
		};
	}

	render() {
		return (
			<Card style={styles.bg}>
				<View style={styles.container}>
					<View
						style={{ flexDirection: "column", justifyContent: "space-around" }}
					>
						<Text style={styles.title}>{this.props.name}</Text>
						<Text style={styles.info}>{this.props.address}</Text>
						<Text style={styles.info}>{this.props.open}</Text>
					</View>
					<View>
						<TouchableOpacity
							onPress={() => {
								console.log(
									"User: ",
									this.props.user,
									"x Biz: ",
									this.props.business
								);
								if (!this.state.favorite) {
									addFavoriteBusiness(this.props.user, this.props.business);
									this.props.addFavorite(this.props.business);
								} else {
									removeFavoriteBusiness(this.props.user, this.props.business);
									this.props.removeFavorite(this.props.business);
								}
								this.setState({ favorite: !this.state.favorite });
							}}
						>
							<Icon
								name={this.state.favorite ? "star" : "star-border"}
								size={30}
								color="white"
							/>
						</TouchableOpacity>
					</View>
				</View>
			</Card>
		);
	}
}

const styles = StyleSheet.create({
	bg: {
		flex: 1,
		backgroundColor: Colors.blue,
		shadowColor: Colors.blueShadow,
		marginBottom: 16,
		width: "100%",
		minHeight: 55
	},
	container: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-start"
	},
	text: {
		width: "80%"
	},
	favBtn: {},
	title: {
		color: "white",
		fontWeight: "bold",
		fontSize: 20,
		marginBottom: 6
	},
	info: {
		color: "white",
		fontSize: 16,
		marginBottom: 6
	}
});

export default connect(
	mapStateToProps,
	{ addFavorite, removeFavorite }
)(BusinessCard);

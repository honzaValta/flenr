/* @flow */

import React, { Component } from "react";
import { View, StyleSheet, Linking } from "react-native";
import { connect } from "react-redux";
import { OpenMapDirections } from "react-native-navigation-directions";
import Icon from "react-native-vector-icons/MaterialIcons";

import { CardButton } from "../basic/CardButton";
import Colors from "./../../constants/Colors";

const mapStateToProps = state => ({
  destination: state.map.currentEvent.location,
  userLocation: state.map.userLocation
});

class ControlBar extends Component {
  render() {
    const { userLocation, destination } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.button}>
          <CardButton
            onPress={() => {
              Linking.openURL("tel:+420724839578");
            }}
          >
            <Icon name="call" size={35} color={Colors.blue} />
          </CardButton>
        </View>
        <View style={styles.button}>
          <CardButton
            onPress={() => {
              Linking.openURL("https://google.com");
            }}
          >
            <Icon name="public" size={35} color={Colors.blue} />
          </CardButton>
        </View>
        <View style={styles.button}>
          <CardButton
            onPress={() => {
              OpenMapDirections(userLocation, destination, "w");
            }}
          >
            <Icon name="navigation" size={35} color={Colors.blue} />
          </CardButton>
        </View>
        <View style={styles.button}>
          <CardButton
            onPress={() => {
              console.log("Share(btn)");
            }}
          >
            <Icon name="share" size={35} color={Colors.blue} />
          </CardButton>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    marginBottom: 16
  },
  button: {
    width: "20%",
    margin: 8
  }
});

export default connect(
  mapStateToProps,
  {}
)(ControlBar);

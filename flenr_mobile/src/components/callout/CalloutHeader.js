import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Colors from "../../constants/Colors";
import { Card } from "../basic";

export const CalloutHeader = props => (
  <View style={styles.header}>
    <Card style={styles.counter}>
      <Text style={{ fontSize: 30, color: "white", fontWeight: "bold" }}>
        {props.coupons}
      </Text>
    </Card>
    <Card style={styles.title}>
      <Text style={{ fontSize: 25, textAlign: 'justify' }}>{props.title}</Text>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
    marginBottom: 16
  },
  counter: {
    backgroundColor: Colors.red,
    shadowColor: Colors.redShadow
  },
  title: {
    flex: 1,
    marginLeft: 8
  }
});

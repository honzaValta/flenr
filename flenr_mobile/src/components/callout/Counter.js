/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Colors from './../../constants/Colors';

export const Counter = ({ size=60 }) => (
  <View style={{...styles.container, width: size, height: size, borderRadius: size/2,}}>
    <Text>42</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "red",
  },
});

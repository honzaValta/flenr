/* @flow */

import React, { Component } from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import { MapView, Location, Permissions } from "expo";
import { connect } from "react-redux";

import {
	showCallout,
	setUserLocation,
	setFoodCategory
} from "./../data/actions";

const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0322; //0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const mapStateToProps = state => ({
	events: state.map.events,
	userId: state.auth.user.uid
});

class Map extends Component {
	constructor(props) {
		super(props);
		console.log("Map rendering");
		this.state = {
			initialRegion: {
				latitude: 0,
				longitude: 0,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			}
		};
		// this.props.markers.map(marker => console.log(marker.location));
	}

	componentDidMount() {
		this.getLocationAsync();
		this.watchPositionAsync();
	}

	async getLocationAsync() {
		const { status } = await Permissions.askAsync(Permissions.LOCATION).catch(
			error => {
				console.log("Permissions not granted: ", error);
				this.setState({
					errorMessage: "Permission to access location was denied"
				});
			}
		);
		if (status !== "granted") {
			console.log("Permissions not granted");
			this.setState({
				errorMessage: "Permission to access location was denied"
			});
		}

		const location = await Location.getCurrentPositionAsync({
			enableHighAccuracy: true
		});

		this.setState({
			initialRegion: {
				latitude: location.coords.latitude,
				longitude: location.coords.longitude,
				latitudeDelta: LATITUDE_DELTA,
				longitudeDelta: LONGITUDE_DELTA
			}
		});
	}

	async watchPositionAsync() {
		Location.watchPositionAsync(
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
			this.updateLocation
		);
	}

	updateLocation = location => {
		// console.log("Position changed");
		this.props.setUserLocation({
			latitude: location.coords.latitude,
			longitude: location.coords.longitude
		});
		this.setState({
			userLocation: {
				latitude: location.coords.latitude,
				longitude: location.coords.longitude
			}
		});
	};

	render() {
		const { Marker } = MapView;

		// console.log("INITIAL-REGION: ", this.state.initialRegion);
		return (
			<View style={styles.container}>
				<MapView
					style={{ flex: 1 }}
					showsUserLocation
					intialRegion={this.state.initialRegion}
					region={this.state.initialRegion}
					provider="google"
					showsMyLocationButton
				>
					{this.props.events.map((event, index) => {
						if (
							!event.usedBy.includes(this.props.userId) &&
							event.timeRange[1].toDate() > new Date()
						) {
							return (
								<Marker
									key={index}
									coordinate={event.location}
									onPress={() => {
										this.props.showCallout(event.id);
									}}
								/>
							);
						}
					})}
				</MapView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	callout: {
		marginTop: 60,
		width: "80%",
		height: "70%"
	}
});

export default connect(
	mapStateToProps,
	{ showCallout, setUserLocation, setFoodCategory }
)(Map);

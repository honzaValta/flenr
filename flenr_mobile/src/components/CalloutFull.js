// /* @flow */
import React, { Component } from "react";
import {
	View,
	Text,
	ScrollView,
	StyleSheet,
	TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { fs } from "../constants/fireStoreInit";
import Colors from "../constants/Colors";
import { hideCallout } from "../data/actions";
import { CalloutHeader } from "./callout";
import BusinessCard from "./callout/BusinessCard";
import UseButton from "./callout/UseButton";
import ControlBar from "./callout/ControlBar";
import { Card } from "./basic";
import { useEvent } from "../data/firebaseHandling";
import { withNavigation } from "react-navigation";

const mapStateToProps = state => ({
	currentEvent: state.map.currentEvent,
	user: state.auth.user.uid
});

class CalloutFull extends Component {
	// constructor(props) {
	// 	super(props);
	// 	this.state = props.currentEvent;
	// }

	render() {
		const { currentEvent, user } = this.props;
		// console.log("Current event");
		// console.log(currentEvent);
		const today = new Date();
		let startTime = currentEvent.timeRange[0].toDate();
		startTime = startTime
			.toTimeString()
			.split(" ")[0]
			.split(":");
		startTime = `${startTime[0]}:${startTime[1]}`;
		let endTime = currentEvent.timeRange[1].toDate();
		endTime = endTime
			.toTimeString()
			.split(" ")[0]
			.split(":");
		endTime = `${endTime[0]}:${endTime[1]}`;
		return (
			<View style={{ marginTop: 40 }}>
				<View style={styles.topCallout}>
					<Card style={styles.timeTag}>
						<Text
							style={{ color: "white" }}
						>{`${startTime} - ${endTime}`}</Text>
					</Card>
					<TouchableOpacity onPress={() => this.props.hideCallout()}>
						<Card style={styles.cancelButton}>
							<Text style={{ color: "white" }}>Close</Text>
						</Card>
					</TouchableOpacity>
				</View>
				<ScrollView style={styles.scrollView}>
					<View style={styles.container}>
						<CalloutHeader
							title={currentEvent.title}
							coupons={currentEvent.coupons}
						/>
						<Card style={styles.description}>
							<Text style={{ color: "grey", fontSize: 16, marginBottom: 8, }}>Description</Text>
							<Text style={{ fontSize: 16, textAlign: "justify" }}>
								{currentEvent.description}
							</Text>
						</Card>
						<BusinessCard
							name={currentEvent.placeName}
							address={currentEvent.address}
							open={currentEvent.open}
						/>
						<ControlBar />
						<UseButton
							disabled={
								!(
									currentEvent.timeRange[0].toDate() < today &&
									currentEvent.timeRange[1].toDate() > today &&
									currentEvent.coupons > 0
								)
							}
							onPress={() => {
								this.props.hideCallout();
								useEvent(currentEvent.id, user);
								this.props.navigation.navigate("Ticket");
							}}
						/>
					</View>
				</ScrollView>
			</View>
		);
	}
}

const CF = withNavigation(CalloutFull);

export default connect(
	mapStateToProps,
	{ hideCallout }
)(CF);

const styles = StyleSheet.create({
	calloutView: {
		position: "absolute",
		bottom: 0,
		width: "100%",
		height: "100%"
	},
	container: {
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "center"
	},
	scrollView: {
		height: "100%",
		backgroundColor: "rgba(245,245,245,1)",
		padding: 12,
		borderTopLeftRadius: 12,
		borderTopRightRadius: 12
	},
	topCallout: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-end",
		maxHeight: 50
	},
	timeTag: {
		marginBottom: 5,
		padding: 4,
		borderRadius: 6,
		marginLeft: 12,
		backgroundColor: Colors.blue
	},
	cancelButton: {
		marginBottom: 5,
		padding: 4,
		borderRadius: 6,
		right: 12,
		backgroundColor: Colors.red
	},
	description: {
		width: "100%",
		marginBottom: 16
	}
});

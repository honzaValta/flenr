import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { Card } from "./Card";

export const CardButton = props => (
  <TouchableOpacity onPress={props.onPress} style={props.style} disabled={props.disabled}>
    <Card style={[styles.default, props.cardStyle]}>{props.children}</Card>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  default: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

import React from "react";
import { View, StyleSheet } from "react-native";

export const Card = props => (
  <View style={[styles.default, props.style]}>{props.children}</View>
);

const styles = StyleSheet.create({
  default: {
    backgroundColor: "white",
    shadowColor: "rgba(201, 201, 201, 0.5)",
    borderRadius: 12,
    shadowRadius: 10,
    shadowOpacity: 1,
    padding: 8,
    shadowOffset: { width: 1, height: 6 },
    elevation: 6
  }
});

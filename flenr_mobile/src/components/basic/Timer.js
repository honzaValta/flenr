import React from "react";
import { StyleSheet } from "react-native";
import TimerCountdown from "react-native-timer-countdown";
import { Card } from "./Card";
import Colors from "../../constants/Colors";

export const Timer = props => (
  <Card style={styles.container}>
    <TimerCountdown
      initialMilliseconds={1000 * props.time}
      onExpire={props.onExpire}
      allowFontScaling={true}
      style={{ fontSize: 70, fontWeight: "bold", color: "white" }}
    />
  </Card>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 25,
    backgroundColor: Colors.red,
    shadowColor: Colors.redShadow
  }
});

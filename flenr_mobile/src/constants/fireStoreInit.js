import * as firebase from "firebase";
import "firebase/firestore"; //for using firestore functions, see https://stackoverflow.com/a/50684682/8236733
import { FIREBASE_CONFIG } from "./../constants/ApiKeys";

// Initialize Firebase
// why in separate file? see https://github.com/zeit/next.js/issues/1999 and https://ilikekillnerds.com/2018/02/solving-issue-firebase-app-named-default-already-exists/

// firebase.initializeApp(firebaseConfig);

try {
  firebase.initializeApp(FIREBASE_CONFIG);

  /*WARN:
    @firebase/firestore:, Firestore (5.0.4):
    The behavior for Date objects stored in Firestore is going to change
    AND YOUR APP MAY BREAK.
    To hide this warning and ensure your app does not break, you need to add the
    following code to your app before calling any other Cloud Firestore methods:

    const firestore = firebase.firestore();
    const settings = {timestampsInSnapshots: true};
    firestore.settings(settings);

    With this change, timestamps stored in Cloud Firestore will be read back as
    Firebase Timestamp objects instead of as system Date objects. So you will also
    need to update code expecting a Date to instead expect a Timestamp. For example:

        // Old:
        const date = snapshot.get('created_at');
        // New:
        const timestamp = snapshot.get('created_at');
        const date = timestamp.toDate();

    Please audit all existing usages of Date when you enable the new behavior. In a
    future release, the behavior will change to the new behavior, so if you do not
    follow these steps, YOUR APP MAY BREAK.
    */
} catch (err) {
  // we skip the "already exists" message which is
  // not an actual error when we're hot-reloading
  if (!/already exists/.test(err.message)) {
    console.error("Firebase initialization error", err.stack);
  }
}

export const fs = firebase.firestore();

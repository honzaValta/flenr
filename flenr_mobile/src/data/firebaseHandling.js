import { fs } from "./../constants/fireStoreInit.js";
import * as firebase from "firebase";

export const createUser = (uid, user) => {
	fs.collection("users")
		.doc(uid)
		.set({
			...user,
			uid: uid,
			favorites: [],
			usedEvents: [],
			location: []
		});
};

export const fetchUser = async uid => {
	// console.log("Fetching user:", uid);
	// const docRef = fs.collection("users").doc(uid);
	// docRef
	// 	.get()
	// 	.then(async doc => {
	// 		if (doc.exists) {
	// 			console.log("User done: ", doc.data());
	// 			const data = await doc.data();
	// 			return data;
	// 		} else {
	// 			// doc.data() will be undefined in this case
	// 			return null;
	// 		}
	// 	})
	// 	.catch(error => {
	// 		return null;
	// 		console.log(error);
	// 	});

	const doc = await fs
		.collection("users")
		.doc(uid)
		.get();
	if (doc.exists) {
		return await doc.data();
	} else {
		return null;
	}
};

export async function getMarkers() {
	const snapshot = await fs.collection("events").get();
	return { markers: snapshot.docs.map(doc => doc.data()) };
}

export const addFavoriteBusiness = (user, business) => {
	fs.collection("users")
		.doc(user)
		.update({
			favorites: firebase.firestore.FieldValue.arrayUnion(business)
		});
};

export const removeFavoriteBusiness = (user, business) => {
	fs.collection("users")
		.doc(user)
		.update({
			favorites: firebase.firestore.FieldValue.arrayRemove(business)
		});
};

export const useEvent = (eventId, userId) => {
	console.log("User: ", userId);
	const decrement = firebase.firestore.FieldValue.increment(-1);
	fs.collection("events")
		.doc(eventId)
		.update({
			usedBy: firebase.firestore.FieldValue.arrayUnion(userId),
			coupons: decrement
		});
	fs.collection("users")
		.doc(userId)
		.update({ usedEvents: firebase.firestore.FieldValue.arrayUnion(eventId) });
};

export const uploadUserLocation = (currentUser, location, time) => {
	fs.collection("users")
		.doc(currentUser)
		.update({
			favorites: fs.FieldValue.arrayUnion({ time, location })
		});
};

export const eventUsed = event => {};

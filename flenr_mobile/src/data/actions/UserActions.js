import * as firebase from "firebase";
import {
  FETCH_USER,
} from "./types";

export const fetchMarkers = () => dispatch => {
  console.log("Fetching user action");
  firebase
    .firestore()
    .collection("users")
    .get()
    .then(snapshot => {
      const markers = snapshot.docs.map(doc => doc.data());
      dispatch({ type: FETCH_MARKERS, value: markers });
      console.log("Fetching done");
    });
};

export const setUserLocation = location => {
  console.log("Setting current location");
  return {
    type: SET_USER_LOCATION,
    value: location
  };
};

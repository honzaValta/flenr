import {
	USER_LOGIN_SUCCESS,
	USER_LOGIN_FAIL,
	SET_USER_UID,
	SET_USER,
	ADD_FAVORITE,
	REMOVE_FAVORITE
} from "./types";

export const setUser = userData => {
	return {
		type: SET_USER,
		value: userData
	};
};

export const setUserUid = userUid => {
	// console.log("Getting user uid");
	return {
		type: SET_USER_UID,
		value: userUid
	};
};

export const addFavorite = id => {
	return {
		type: ADD_FAVORITE,
		value: id
	};
};

export const removeFavorite = id => {
	return {
		type: REMOVE_FAVORITE,
		value: id
	};
};

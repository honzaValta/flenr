import { TICK, RESET_TIMER } from './types';

export const startTimer = () => (dispatch, getState) => {
  console.log('Starting timer');
  // let timer = null;
  clearInterval(timer);
  const timer = setInterval(() => {
      const { ticket } = getState();
      dispatch(tick());
      // const { ticket } = getState();
      console.log('Time:  ', ticket.time);
      if (ticket.time === 1) {
        console.log('End time');
        clearInterval(timer);
      }
  }, 1000);
};

const tick = () => ({ type: TICK });

export const resetTimer = () => {
  return {
    type: RESET_TIMER,
  };
};

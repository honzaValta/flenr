import * as firebase from "firebase";
import {
	FETCH_MARKERS,
	SHOW_CALLOUT,
	HIDE_CALLOUT,
	SET_USER_LOCATION,
	SET_FOOD_CATEGORY,
	SET_DRINK_CATEGORY,
	SET_EVENTS
} from "./types";

export const fetchMarkers = () => dispatch => {
	console.log("Starting to fetch markers");
	firebase
		.firestore()
		.collection("events")
		.get()
		.then(snapshot => {
			const markers = snapshot.docs.map(doc => doc.data());
			dispatch({ type: FETCH_MARKERS, value: markers });
			console.log("Fetching markers done");
		});
};

export const setEvents = (events) => {
	return {
		type: SET_EVENTS,
		value: events
	}
}

export const setUserLocation = location => {
	console.log("Setting current location");
	return {
		type: SET_USER_LOCATION,
		value: location
	};
};

export const showCallout = id => {
	// console.log("Showing callout");
	return {
		type: SHOW_CALLOUT,
		value: id
	};
};

export const hideCallout = () => {
	return {
		type: HIDE_CALLOUT
	};
};

export const setFoodCategory = () => {
	return {
		type: SET_FOOD_CATEGORY
	};
};

export const setDrinkCategory = () => {
	return {
		type: SET_DRINK_CATEGORY
	};
};

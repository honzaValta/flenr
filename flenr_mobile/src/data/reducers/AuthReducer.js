import {
	USER_LOGIN_SUCCESS,
	USER_LOGIN_FAIL,
	SET_USER_UID,
	SET_USER,
	ADD_FAVORITE,
	REMOVE_FAVORITE
} from "../actions/types";

const INITIAL_STATE = { status: "loading", user: { uid: "x" } };

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		// case USER_LOGIN_SUCCESS:
		// 	console.log("status: ", action.value);
		// 	return { ...state, uid: action.value, status: "signedIn" };
		// case USER_LOGIN_FAIL:
		// 	return { ...state, status: "signedOut" };
		// case SET_USER_UID:
		// 	return { ...state, userUid: action.value };
		case SET_USER:
			console.log("Setting user: ", action.value);
			return { ...state, user: action.value };
		case ADD_FAVORITE:
			const favoriteAdded = state.user;
			favoriteAdded.favorites.push(action.value);
			// console.log("Favorites added: ", favoriteAdded.favorites);
			return { ...state, user: favoriteAdded };
		case REMOVE_FAVORITE:
			const favoriteRemoved = state.user;
			favoriteRemoved.favorites = favoriteRemoved.favorites.filter(item => {
				return item !== action.value;
			});
			// console.log("Favorites removed: ", favoriteRemoved.favorites);
			return { ...state, user: favoriteRemoved };
		default:
			return state;
	}
};

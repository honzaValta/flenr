import {
	FETCH_MARKERS,
	SHOW_CALLOUT,
	HIDE_CALLOUT,
	SET_USER_LOCATION,
	SET_FOOD_CATEGORY,
	SET_DRINK_CATEGORY,
	SET_EVENTS
} from "../actions/types";

const INITIAL_STATE = {
	markers: [],
	currentMarkers: [],
	callout: false,
	currentEvent: null,
	userLocation: {},
	category: "food",
	events: []
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SET_EVENTS:
			if (state.currentEvent) {
				// console.log("Y");
				currentEvent = action.value.find(obj => {
					return obj.id === state.currentEvent.id;
				});
				// console.log(currentEvent);
				return { ...state, events: action.value, currentEvent };
			} else {
				// console.log("N");
				return { ...state, events: action.value };
			}

		case FETCH_MARKERS:
			return {
				...state,
				markers: action.value,
				currentMarkers: action.value.filter(
					marker => marker.category === "food"
				)
			};
		case SHOW_CALLOUT:
			currentEvent = state.events.find(obj => {
				return obj.id === action.value;
			});
			return {
				...state,
				callout: true,
				currentEvent
			};
		case HIDE_CALLOUT:
			return { ...state, callout: false };
		case SET_USER_LOCATION:
			return { ...state, userLocation: action.value };
		case SET_FOOD_CATEGORY:
			return {
				...state,
				category: "food",
				currentMarkers: state.markers.filter(
					marker => marker.category === "food"
				)
			};
		case SET_DRINK_CATEGORY:
			return {
				...state,
				category: "drink",
				currentMarkers: state.markers.filter(
					marker => marker.category === "drink"
				)
			};
		default:
			return state;
	}
};

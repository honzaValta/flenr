import { combineReducers } from 'redux';
import MapReducer from './MapReducer';
import AuthReducer from './AuthReducer';

export default combineReducers({
  auth: AuthReducer,
  map: MapReducer,
});

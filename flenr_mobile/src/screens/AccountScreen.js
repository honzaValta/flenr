import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { Card, CardButton } from "../components/basic";
import Colors from "../constants/Colors";
import { MaterialIcons } from "@expo/vector-icons";
import { connect } from "react-redux";

const mapStateToProps = state => ({
	account: state.auth,
	events: state.map.events
});

class AccountScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const user = this.props.account.user;
		let food = this.props.events.filter(event => event.category === "food");
		let drink = this.props.events.filter(event => event.category === "drink");
		let cafe = this.props.events.filter(event => event.category === "cafe");
		food = food.filter(event => event.usedBy.includes(user.uid)).length;
		drink = drink.filter(event => event.usedBy.includes(user.uid)).length;
		cafe = cafe.filter(event => event.usedBy.includes(user.uid)).length;
		return (
			<View style={styles.container}>
				<Card style={styles.header}>
					<Text style={{ color: "white", fontWeight: "bold", fontSize: 35 }}>
						{user.name}
					</Text>
				</Card>
				<ScrollView style={{ padding: 12, height: "100%" }}>
					<View style={styles.stats}>
						<Card
							style={{
								backgroundColor: Colors.yellow,
								shadowColor: Colors.yellowShadow,
								flexDirection: "row",
								justifyContent: "space-around",
								alignItems: "center",
								width: "25%"
							}}
						>
							<MaterialIcons name="restaurant-menu" size={25} color="white" />
							<Text style={{ color: "white", fontSize: 25 }}>{food}</Text>
						</Card>
						<Card
							style={{
								backgroundColor: Colors.green,
								shadowColor: Colors.greenShadow,
								flexDirection: "row",
								justifyContent: "space-around",
								alignItems: "center",
								width: "25%"
							}}
						>
							<MaterialIcons name="local-cafe" size={25} color="white" />
							<Text style={{ color: "white", fontSize: 25 }}>{cafe}</Text>
						</Card>
						<Card
							style={{
								backgroundColor: Colors.purple,
								shadowColor: Colors.purpleShadow,
								flexDirection: "row",
								justifyContent: "space-around",
								alignItems: "center",
								width: "25%"
							}}
						>
							<MaterialIcons name="local-bar" size={25} color="white" />
							<Text style={{ color: "white", fontSize: 25 }}>{drink}</Text>
						</Card>
					</View>
					<CardButton
						onPress={() => {
							this.props.navigation.navigate("History");
						}}
						style={styles.button}
						cardStyle={{
							flexDirection: "row",
							justifyContent: "space-between",
							alignItems: "center",
							minHeight: 55,
							marginBottom: 30
						}}
					>
						<Text style={{ fontSize: 20, fontWeight: "bold" }}>
							Event History
						</Text>
						<MaterialIcons name="navigate-next" size={25} color="black" />
					</CardButton>
					<Card style={{ padding: 16 }}>
						<Text
							style={{
								fontSize: 20,
								fontWeight: "bold",
								textAlign: "center",
								marginBottom: 20
							}}
						>
							About this app
						</Text>
						<Text style={{ fontSize: 16, textAlign: "justify" }}>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
							at venenatis elit. Proin rutrum malesuada felis a varius.
							Suspendisse semper nulla sit amet lorem porttitor aliquet. Nullam
							quis dolor blandit, venenatis justo ac, imperdiet sapien. Etiam ut
							interdum mauris, vel blandit magna. Fusce eros augue, laoreet et
							odio eget, iaculis semper ex. In non elit nec magna blandit
							accumsan quis in diam. Donec rutrum mauris placerat, venenatis
							odio sit amet, placerat lectus.
						</Text>
					</Card>
				</ScrollView>
			</View>
		);
	}
}

export default connect(
	mapStateToProps,
	{}
)(AccountScreen);

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		// flex: 1,
		padding: 12,
		paddingTop: "9%",
		backgroundColor: "rgba(245,245,245,1)",
		height: "100%",
		top: 0,
		bottom: 0
	},
	header: {
		backgroundColor: Colors.cyan,
		shadowColor: Colors.cyanShadow,
		marginBottom: 10
	},
	stats: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 30,
		marginTop: 5
	},
	button: {
		flex: 1
	}
});

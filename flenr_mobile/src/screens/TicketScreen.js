import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import { Card } from "../components/basic";
import Colors from "../constants/Colors";
import { setFoodCategory, setDrinkCategory } from "./../data/actions";
import { Timer } from "../components/basic";
import Icon from "react-native-vector-icons/MaterialIcons";
import { bold } from "ansi-colors";

const mapStateToProps = state => ({
	title: state.map.currentEvent.title,
	placeName: state.map.currentEvent.placeName,
	user: state.auth.user
});

class TicketScreen extends Component {
	render() {
		const { title, placeName, user } = this.props;
		return (
			<ScrollView style={styles.container}>
				// Title
				<Card
					style={{
						flex: 1,
						justifyContent: "center",
						alignItems: "center",
						marginBottom: 25
					}}
				>
					<Text style={{ fontSize: 25, fontWeight: "bold" }}>{title}</Text>
				</Card>
				// Timer
				<Timer
					onExpire={() => {
						this.props.navigation.navigate("Home");
					}}
					time={10}
				/>
				// Account info
				<Card style={styles.profile}>
					// Avatar
					<Icon name="account-circle" size={120} color="black" />
					// Name
					<View>
						<Text style={styles.name}>{user.first_name}</Text>
						<Text style={styles.name}>{user.last_name}</Text>
					</View>
				</Card>
				<Card
					style={{
						flexDirection: "row",
						alignItems: "center",
						padding: 10,
						marginBottom: 30
					}}
				>
					<Icon name="location-on" size={35} color={Colors.blue} />
					<Text style={{ fontSize: 20, marginLeft: 15, fontWeight: "bold" }}>
						{placeName}
					</Text>
				</Card>
				<Card>
					<Text style={{ color: "grey", fontSize: 16 }}>Description</Text>
					<Text style={{ fontSize: 16, textAlign: "justify" }}>
						{currentEvent.description}
					</Text>
				</Card>
				// Map/animation check = compass
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		width: "100%",
		height: "100%",
		padding: 12,
		paddingTop: "9%",
		backgroundColor: "rgba(245,245,245,1)"
	},
	center: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	profile: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",
		marginBottom: 30
	},
	name: {
		fontSize: 25,
		fontWeight: "bold",
		margin: 12
	}
});

export default connect(
	mapStateToProps,
	{ setFoodCategory, setDrinkCategory }
)(TicketScreen);

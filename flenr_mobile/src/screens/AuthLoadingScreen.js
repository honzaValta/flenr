import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import * as firebase from "firebase";
import Loader from "react-native-modal-loader";
import { fetchMarkers, setUser, setEvents } from "../data/actions";
import { fetchUser } from "../data/firebaseHandling";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { fs } from "../constants/fireStoreInit.js";

class AuthLoadingScreen extends Component {
	async getEvents() {
		fs.collection("events").onSnapshot(snapshot => {
			const events = snapshot.docs.map(doc => doc.data());
			events.sort((ts1, ts2) => {
				if (ts1.timeRange[0] > ts2.timeRange[0]) return 1;
				if (ts1.timeRange[0] < ts2.timeRange[0]) return -1;
				return 0;
			});
			this.props.setEvents(events);
			// console.log('Current data: ', events);
		});
	}

	checkUser() {
		// firebase.auth().signOut();
		firebase.auth().onAuthStateChanged(async user => {
			if (user) {
				// User is signed in.
				this.getEvents();
				const userInfo = await fetchUser(user.uid);
				this.props.setUser(await userInfo);
				this.props.navigation.navigate("Home");
			} else {
				// No user is signed in.
				this.getEvents();
				// this.fetchUser("ooyGpQKUYTeaFFMY4oy38vXZolE3");
				this.props.navigation.navigate("Auth");
			}
		});
	}

	render() {
		this.checkUser();
		return (
			<View>
				<Loader size="large" color={Colors.cyan} loading />
			</View>
		);
	}
}

export default connect(
	null,
	{ fetchMarkers, setUser, setEvents }
)(AuthLoadingScreen);

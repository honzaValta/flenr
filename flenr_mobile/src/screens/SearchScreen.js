import React, { Component } from "react";
import {
	View,
	Text,
	StyleSheet,
	ScrollView,
	TextInput,
	TouchableOpacity,
	Keyboard
} from "react-native";
import { connect } from "react-redux";
import { Card } from "../components/basic";
import { EventCard } from "../components/EventCard";
import Colors from "../constants/Colors";
import Icon from "react-native-vector-icons/MaterialIcons";
import { showCallout } from "../data/actions";

const mapStateToProps = state => ({
	events: state.map.events,
	userId: state.auth.user.uid
});

class SearchScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			search: ""
		};
	}

	render() {
		return (
			<View style={styles.container}>
				<Card style={styles.header}>
					<View style={styles.searchContainer}>
						<TextInput
							style={styles.searchInput}
							placeholder="Hledat akci"
							onChangeText={search => this.setState({ search })}
							value={this.state.search}
							onSubmitEditing={Keyboard.dismiss}
							returnKeyType={"done"}
						/>
					</View>
					<TouchableOpacity onPress={() => this.setState({ search: "" })}>
						<Icon name="cancel" size={25} color="white" />
					</TouchableOpacity>
				</Card>
				<ScrollView style={{ width: "100%", height: "100%" }}>
					{this.props.events.map((event, index) => {
						if (
							event.timeRange[1].toDate() > new Date() &&
							!event.usedBy.includes(this.props.userId) &&
							event.title
								.toLowerCase()
								.includes(this.state.search.toLowerCase())
						) {
							let startTime = event.timeRange[0].toDate();
							startTime = startTime
								.toTimeString()
								.split(" ")[0]
								.split(":");
							startTime = `${startTime[0]}:${startTime[1]}`;
							let endTime = event.timeRange[1].toDate();
							endTime = endTime
								.toTimeString()
								.split(" ")[0]
								.split(":");
							endTime = `${endTime[0]}:${endTime[1]}`;
							let day = "" + event.timeRange[0].toDate().getDate();
							let month = "." + (event.timeRange[0].toDate().getMonth() + 1);
							return (
								<TouchableOpacity
									key={index}
									onPress={() => {
										this.props.navigation.goBack();
										this.props.showCallout(event.id);
									}}
								>
									<EventCard
										key={index}
										title={event.title}
										business={event.business}
										date={day + month}
										duration={`${startTime} - ${endTime}`}
									/>
								</TouchableOpacity>
							);
						}
					})}
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		marginTop: "9%",
		padding: 12
	},
	header: {
		backgroundColor: Colors.red,
		shadowColor: Colors.redShadow,
		padding: 12,
		margin: 20,
		marginTop: 0,
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-around",
		alignItems: "center"
	},
	searchContainer: {
		backgroundColor: "rgba(240, 240, 240, 0.3)",
		borderRadius: 8,
		height: 40,
		width: "90%"
	},
	searchInput: {
		width: "100%",
		height: "100%",
		color: "white",
		fontSize: 20,
		fontWeight: "bold",
		padding: 6,
		margin: "auto"
	}
});

export default connect(
	mapStateToProps,
	{ showCallout }
)(SearchScreen);

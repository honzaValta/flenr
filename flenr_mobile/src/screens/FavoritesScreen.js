import React, { Component } from "react";
import {
	View,
	Text,
	StyleSheet,
	ScrollView,
	TouchableOpacity
} from "react-native";
import { Card } from "../components/basic";
import { EventCard } from "../components/EventCard";
import Colors from "../constants/Colors";
import { showCallout } from "../data/actions";
import { connect } from "react-redux";

const mapStateToProps = state => ({
	events: state.map.events,
	favorites: state.auth.user.favorites
});

class FavoritesScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<View style={styles.container}>
				<Card style={styles.header}>
					<Text style={{ color: "white", fontSize: 35, fontWeight: "bold" }}>
						Favorites
					</Text>
				</Card>
				<ScrollView style={{ width: "100%", height: "100%" }}>
					{this.props.events.map((event, index) => {
						if (
							this.props.favorites.includes(event.ownerId) &&
							event.timeRange[1].toDate() > new Date()
						) {
							let startTime = event.timeRange[0].toDate();
							startTime = startTime
								.toTimeString()
								.split(" ")[0]
								.split(":");
							startTime = `${startTime[0]}:${startTime[1]}`;
							let endTime = event.timeRange[1].toDate();
							endTime = endTime
								.toTimeString()
								.split(" ")[0]
								.split(":");
							endTime = `${endTime[0]}:${endTime[1]}`;
							let day = "" + event.timeRange[0].toDate().getDate();
							let month = "." + (event.timeRange[0].toDate().getMonth() + 1);
							return (
								<TouchableOpacity
									key={index}
									onPress={() => {
										this.props.navigation.goBack();
										this.props.showCallout(event.id);
									}}
								>
									<EventCard
										key={index}
										title={event.title}
										business={event.business}
										date={day + month}
										duration={`${startTime} - ${endTime}`}
									/>
								</TouchableOpacity>
							);
						}
					})}
				</ScrollView>
			</View>
		);
	}
}

export default connect(
	mapStateToProps,
	{ showCallout }
)(FavoritesScreen);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		marginTop: "9%",
		padding: 12
	},
	header: {
		backgroundColor: Colors.red,
		shadowColor: Colors.redShadow,
		padding: 12,
		margin: 20,
		marginTop: 0,
		width: "100%",
		alignItems: "center"
	}
});

/* @flow */

import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import * as firebase from "firebase";
import "firebase/firestore";
import { connect } from "react-redux";
import { fetchUser, fetchMarkers, showCallout } from "./../data/actions";
import ControlPanel from "../components/ControlPanel";
import Map from "./../components/Map";
import CalloutFull from "../components/CalloutFull";

const mapStateToProps = state => ({
	currentEvent: state.map.currentEvent,
	callout: state.map.callout,
	logInStatus: state.auth.status,
	user: state.auth.user
});

class MainScreen extends Component {
	constructor(props) {
		super(props);
		console.log("User main: ", this.props.user);
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.map}>
					<Map />
				</View>
				<View style={styles.controlPanel}>
					<ControlPanel />
				</View>
				{/* <View> */}
				{this.props.callout === true && (
					<CalloutFull navigation={this.props.navigation} />
				)}
				{/* </View> */}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	map: {
		position: "absolute",
		width: "100%",
		height: "100%",
		zIndex: 0
	},
	controlPanel: {
		marginTop: 45,
		alignSelf: "center",
		height: 54,
		width: "90%",
		zIndex: 1
	},
	callout: {
		flex: 1,
		flexDirection: "column-reverse",
		width: "100%",
		borderTopLeftRadius: 12,
		borderTopRightRadius: 12,
		zIndex: 1
	}
});

export default connect(
	mapStateToProps,
	{ fetchUser, fetchMarkers, showCallout }
)(MainScreen);

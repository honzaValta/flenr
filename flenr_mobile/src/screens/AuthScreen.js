import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import * as firebase from "firebase";
import { createUser, fetchUser } from "./../data/firebaseHandling";
import { FACEBOOK_APP_ID } from "./../constants/ApiKeys";
import { Facebook } from "expo";
import { connect } from "react-redux";
import { setUser } from "../data/actions";
import Colors from "../constants/Colors";
import { Card } from "../components/basic";

class AuthScreen extends Component {
	async signUp() {
		console.log("Signing up...");
		const { type, token } = await Facebook.logInWithReadPermissionsAsync(
			FACEBOOK_APP_ID,
			{ permissions: ["public_profile", "email"] }
		);
		console.log("Type: ", type);
		if (type === "success") {
			const credential = firebase.auth.FacebookAuthProvider.credential(token);
			// Firebase authentication
			firebase
				.auth()
				.signInAndRetrieveDataWithCredential(credential)
				.catch(error => {
					Alert.alert(
						"Error, restart the app",
						error.message,
						[{ text: "OK", onPress: () => console.log("OK Pressed") }],
						{ cancelable: false }
					);
				});
			// Facebook profile info
			const response = await fetch(
				`https://graph.facebook.com/me?fields=id,name,email,first_name,last_name&access_token=${token}`
			);
			// console.log("FB: ", await response.json());
			// Creating user in firestore on first time login
			// console.log(userId);
			firebase.auth().onAuthStateChanged(user => {
				if (user) {
					const userId = user.uid;
					firebase
						.firestore()
						.collection("users")
						.doc(userId)
						.get()
						.then(async doc => {
							console.log("Doc: ", !doc.exists);
							if (!doc.exists) {
								const userData = await response.json();
								console.log("Creating user", userId, userData);
								createUser(userId, userData);
							}
						})
						.then(async () => {
							console.log("Setting User");
							const userInfo = await fetchUser(userId);
							this.props.setUser(await userInfo);
							this.props.navigation.navigate("Home");
						});
				}
			});
		}
	}

	async testLogin() {
		const userInfo = await fetchUser("DlTtaoTojXopdjWwyOkN");
		this.props.setUser(await userInfo);
		this.props.navigation.navigate("Home");
	}

	fakeLogin = () => {
		this.props.navigation.navigate("Home");
	};

	render() {
		return (
			<View style={styles.container}>
				<Card style={styles.logo}>
					<Text style={styles.logoText}>logo</Text>
				</Card>
				<TouchableOpacity
					style={styles.fbBtn}
					onPress={() => {
						this.signUp();
					}}
				>
					<Text style={{ color: "white", fontSize: 20 }}>
						Login with facebook
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.testLogin()}
					style={{ margin: 50 }}
				>
					<Text style={{ fontSize: 20 }}>Test login</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

export default connect(
	null,
	{ setUser }
)(AuthScreen);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
		alignItems: "center",
		backgroundColor: "rgba(255, 255, 255, 1)",
		padding: 5
	},
	logo: {
		borderRadius: 100,
		backgroundColor: Colors.cyan,
		shadowColor: Colors.cyanShadow,
		width: "50%",
		height: 60,
		justifyContent: "center",
		alignItems: "center",
		padding: 4,
		marginRight: 10,
		marginTop: 200
	},
	logoText: {
		fontSize: 30,
		fontWeight: "800",
		textAlign: "center",
		color: "white"
	},
	fbBtn: {
		backgroundColor: "#3C5A99",
		padding: 16,
		borderRadius: 8,
		position: "absolute",
		bottom: 75
	}
});

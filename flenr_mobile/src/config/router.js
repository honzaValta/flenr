/* @flow weak */

import {
	createSwitchNavigator,
	createStackNavigator,
	createAppContainer
} from "react-navigation";
import AuthLoadingScreen from "../screens/AuthLoadingScreen";
import AuthScreen from "../screens/AuthScreen";
import MainScreen from "../screens/MainScreen";
import TicketScreen from "../screens/TicketScreen";
import AccountScreen from "../screens/AccountScreen";
import FavoritesScreen from "../screens/FavoritesScreen";
import HistoryScreen from "..//screens/HistoryScreen";
import SearchScreen from "../screens/SearchScreen";

const AppStack = createStackNavigator({
	Home: {
		screen: MainScreen,
		navigationOptions: {
			header: null
		}
	},
	Account: {
		screen: AccountScreen,
		navigationOptions: {
			header: null
		}
	},
	Ticket: {
		screen: TicketScreen,
		navigationOptions: {
			header: null
		}
	},
	Favorites: {
		screen: FavoritesScreen,
		navigationOptions: {
			header: null
		}
	},
	History: {
		screen: HistoryScreen,
		navigationOptions: {
			header: null
		}
	},
	Search: {
		screen: SearchScreen,
		navigationOptions: {
			header: null
		}
	}
});

const AuthStack = createStackNavigator({
	Auth: {
		screen: AuthScreen,
		navigationOptions: {
			header: null
		}
	}
});

const AuthLoadingStack = createStackNavigator({
	AuthLoading: {
		screen: AuthLoadingScreen,
		navigationOptions: {
			header: null
		}
	}
});

export const AppScreens = createAppContainer(
	createSwitchNavigator(
		{
			AuthLoading: AuthLoadingStack,
			App: AppStack,
			Auth: AuthStack
		},
		{
			initialRouteName: "AuthLoading"
		}
	)
);
